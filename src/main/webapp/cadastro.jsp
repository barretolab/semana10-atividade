<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="edu.ifsp.lojinha.modelo.Cliente"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Cadastro</title>
</head>

<body>
	<c:choose>
		<c:when test="${not empty requestScope.cliente}">

			<p>Cadastro realizado com sucesso!</p>
			<p>
				Nome:${requestScope.cliente.getNome()} <br>
				E-mail:${requestScope.cliente.getEmail()}
			</p>
		</c:when>
		<c:otherwise>
	</c:otherwise>
	</c:choose>

</body>
</html>